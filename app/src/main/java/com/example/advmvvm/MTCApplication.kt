package com.example.advmvvm

import android.app.Application
import com.lib.MTC.MTC
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by anhdv31 on 2/17/2022.
 */
@HiltAndroidApp
class MTCApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        MTC.SDK_init();
        MTC.SetDebug();
    }
}