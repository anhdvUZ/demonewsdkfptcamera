package com.example.advmvvm.model

import android.os.Parcelable

/**
 * Created by anhdv31 on 2/16/2022.
 */
data class HomeObj (
    val icon: Int,
    val title : String,
    val description : String)