package com.example.advmvvm.model

import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 2/23/2022.
 */
data class UserInfo(
    @SerializedName("user")
    val uName: String,
    @SerializedName("group")
    val groupName: String
)