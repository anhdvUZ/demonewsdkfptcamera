package com.example.advmvvm.ui.videoColor.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.videoColor.main.adapter.VideoColorItemViewModel
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.ui.videoEncode.encode.adapter.VideoEncodeItemViewModel
import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.VideoColorControl
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/28/2022.
 */
@HiltViewModel
class VideoColorViewModel @Inject constructor(
    private val videoColorRepository: VideoColorRepository
): ViewModel(){
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<VideoColorEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<VideoColorEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(videoColorRepository.getVideoColors())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(videoColorControls : List<VideoColorControl>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        videoColorControls.forEach {
            AppLog.d("VideoColorViewModel","ANHDV create VideoColorItemViewModel--> " + Gson().toJson(it))
            viewData.add(VideoColorItemViewModel(it))
        }
        return viewData
    }
    
    open fun onApplyClicked(){
        val newConfig = (_data.value?.get(0) as VideoColorItemViewModel).videoColorControl
        val res = videoColorRepository.setVideoColors(newConfig)
        onApplyVideoColorConfigDone(res)
        Log.d("VideoColorViewModel","onApplyClicked res " + res)
    }
    
    private fun onApplyVideoColorConfigDone(res : Int){
        _events.postValue(Event(VideoColorEvent.ApplyVideoColorConfig(res)))
    }
}

sealed class VideoColorEvent{
    data class ApplyVideoColorConfig(val result: Int) : VideoColorEvent()
}