package com.example.advmvvm.ui.user.userManager

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.model.UserInfo
import com.example.advmvvm.ui.user.userManager.adapter.UserManagerItemViewModel
import com.example.advmvvm.utils.AppLog
import com.lib.MTC.MTC
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/22/2022.
 */

@HiltViewModel
class UserManagerViewModel @Inject constructor(
    private val userManagerRepository: UserManagerRepository
):ViewModel(){
    val data : LiveData<List<BaseItemViewModel>>
    get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<UserManagerItemEvent>>
    get() = _events
    private val _events = MutableLiveData<Event<UserManagerItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            //val result = arrayOfNulls<String>(10)
            //val res = MTC.DEV_GetUserInfo(MTC.loginID, result)
            val viewData = createViewModel(userManagerRepository.getUsersInfo())
            //val viewData = createViewModel(userManagerRepository.getUsersInfoRawData())
            AppLog.d("ANHDV","loadData() " + userManagerRepository.getUsersInfo().size)
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listUserInfo: List<UserInfo>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listUserInfo.forEach {
            viewData.add(UserManagerItemViewModel(it.uName,it.uName,::onItemUserManagerClicked))
        }
        return viewData
    }
    
    private fun onItemUserManagerClicked(uName  :String){
        _events.postValue(Event(UserManagerItemEvent.SwitchPage(uName)))
    }
}
sealed class UserManagerItemEvent{
    data class SwitchPage(val uname : String) : UserManagerItemEvent()
}