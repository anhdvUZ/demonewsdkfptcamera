package com.example.advmvvm.ui.home.adapter

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/17/2022.
 */
class HomeItemViewModel(
    val id : Int,
    val title : String,
    val description : String,
    val icon : Int,
    val onItemClick : (Int) -> Unit
): BaseItemViewModel{
    override val layoutId: Int = R.layout.item_home
    
    override val viewType: Int
        get() = super.viewType
    
    fun onClick(){
        onItemClick(id);
    }
}