package com.example.advmvvm.ui.network.netCommonActive

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentNetCommonActiveBinding
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonAciveViewHolder
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewModel
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NetCommonActiveFragment : DataBindingBaseFragment<FragmentNetCommonActiveBinding,BaseViewModel>(BaseViewModel::class.java) {
    
    private val mViewModel : NetCommonActiveViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_net_common_active
    }
    
    override fun initializeController() {
        binding.netCommonActiveViewModel = mViewModel
        mViewModel.event.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(netCommonActiveEvents: NetCommonActiveEvents?) {
        when(netCommonActiveEvents){
            is NetCommonActiveEvents.ApplyNetCommonActive -> {
                val res = netCommonActiveEvents.result
                AppLog.d(TAG,"NetCommonActiveEvents " + res)
                Toast.makeText(context,"NetCommonActiveConfig " + res, Toast.LENGTH_LONG).show()
            }
        }
    }
}