package com.example.advmvvm.ui.user.userManager

import com.example.advmvvm.model.UserInfo
import com.example.advmvvm.ui.home.adapter.HomeData
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.lib.MTC.MTC

/**
 * Created by anhdv31 on 2/22/2022.
 */

class UserManagerRepository {
    val gson = Gson()
    fun getUsersInfo(): List<UserInfo> {
        var jsonString = arrayOfNulls<String>(10)
        val res = MTC.DEV_GetUserInfo(MTC.loginID, jsonString)
        val typeToken = object : TypeToken<List<UserInfo>>() {}.type
        return gson.fromJson(jsonString[0], typeToken)
    }
    
    fun getUsersInfoRawData():List<UserInfo>{
        val typeToken = object: TypeToken<List<UserInfo>>(){}.type
        return gson.fromJson(getStringUserListData(),typeToken)
    }
    
    private fun getStringUserListData() : String {
        return "[{\"user\":\"admin\",\"group\":\"admin\"},{\"user\":\"user\",\"group\":\"admin\"}]".trimIndent()
    }
}