package com.example.advmvvm.ui.videoEncode.encode

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode

/**
 * Created by anhdv31 on 2/23/2022.
 */
class VideoEncodeRepository {
    
    fun getVideoEncodes():List<Encode>{
        val encodes = arrayListOf<Encode>()
        val encode = Encode()
        val byteObj = Futils.ObjToBytes(encode)
        val res : Int = MTC.DEV_GetConfigEncode(MTC.loginID,byteObj)
        AppLog.d("VideoEncodeRepository","ANHDV DEV_GetConfigEncode --> loginId " + MTC.loginID + " -- " + res)
        if (res >= 0){
            val retGetEncodeInfo = Futils.BytesToObj(encode,byteObj)
            AppLog.d("VideoEncodeRepository","ANHDV --> " +Gson().toJson(encode))
            encodes.add(encode)
        }
        return encodes
    }
    
    fun setVideoEncodes(encode: Encode): Int {
        val byteObj = Futils.ObjToBytes(encode)
        AppLog.d("VideoEncodeRepository","ANHDV setVideoEncodes --> " +Gson().toJson(encode))
        return MTC.DEV_SetConfigEncode(MTC.loginID, byteObj)
    }
}