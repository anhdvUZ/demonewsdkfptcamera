package com.example.advmvvm.ui.user.modifyPassword

import android.view.View
import android.widget.Toast
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentModifyPasswordBinding
import com.example.advmvvm.utils.AppLog
import com.lib.MTC.MTC

class ModifyPasswordFragment : DataBindingBaseFragment<FragmentModifyPasswordBinding, BaseViewModel>(BaseViewModel::class.java), ItemClickListener {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_modify_password
    }
    
    override fun initializeController() {
        binding.itemClickListener = this
    }
    
    override fun onItemClickListener(view: View) {
        when (view.id) {
            R.id.btn_ok -> {
                val uName = binding.tilUsername.editText?.text.toString()
                val oldPwd = binding.tilOldPwd.editText?.text.toString()
                val newPwd = binding.tilNewPwd.editText?.text.toString()
                if (oldPwd.isEmpty() || newPwd.isEmpty()) {
                    Toast.makeText(context, "Password is Empty!!!!!!!!", Toast.LENGTH_LONG).show()
                } else {
                    val res = MTC.DEV_ModifyPassword(MTC.loginID, uName, oldPwd, newPwd)
                    Toast.makeText(context, "ModifyPassword res " + res, Toast.LENGTH_LONG).show()
                    AppLog.d(TAG, "ANHDV ModifyPassword " + res)
                }
            }
        }
    }
    
}