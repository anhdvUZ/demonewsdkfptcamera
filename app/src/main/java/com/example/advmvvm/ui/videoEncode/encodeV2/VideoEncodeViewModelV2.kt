package com.example.advmvvm.ui.videoEncode.encodeV2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 8/1/2022.
 */
@HiltViewModel
class VideoEncodeViewModelV2  @Inject constructor(
    private val videoEncodeRepositoryV2: VideoEncodeRepositoryV2
): ViewModel(){
    
    val encodes: LiveData<List<Encode>>
        get() = _encodes
    private val _encodes = MutableLiveData<List<Encode>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        AppLog.d("VideoEncodeViewModelV2","ANHDV loadData()")
        _encodes.postValue(videoEncodeRepositoryV2.getVideoEncodes())
    }
    
    fun setData(encode: Encode?) {
        var res = videoEncodeRepositoryV2.setVideoEncodes(encode)
        if (res>=0){
            _encodes.postValue(arrayListOf(encode!!))
        }
        AppLog.d("VideoEncodeViewModelV2","ANHDV setData() " + res)
    }
}