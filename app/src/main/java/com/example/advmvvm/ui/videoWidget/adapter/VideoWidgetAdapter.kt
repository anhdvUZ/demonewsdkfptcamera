package com.example.advmvvm.ui.videoWidget.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 3/1/2022.
 */
class VideoWidgetAdapter : RecyclerView.Adapter<VideoWidgetViewHolder>(){
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoWidgetViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return VideoWidgetViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: VideoWidgetViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class VideoWidgetViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root){
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.videoWidgetItemViewModel, itemViewModel)
    }
}

@BindingAdapter("videoWidgetItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): VideoWidgetAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is VideoWidgetAdapter) {
        recyclerView.adapter as VideoWidgetAdapter
    } else {
        val bindableRecyclerAdapter = VideoWidgetAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}
