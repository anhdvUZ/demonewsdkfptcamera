package com.example.advmvvm.ui.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_user
    }
    
    override fun initializeComponents() {}
}