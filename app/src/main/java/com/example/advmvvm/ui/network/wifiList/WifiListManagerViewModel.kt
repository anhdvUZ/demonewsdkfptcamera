package com.example.advmvvm.ui.network.wifiList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.model.UserInfo
import com.example.advmvvm.ui.network.wifiList.adapter.WifiListItemViewModel
import com.example.advmvvm.ui.user.userManager.UserManagerItemEvent
import com.example.advmvvm.ui.user.userManager.adapter.UserManagerItemViewModel
import com.example.advmvvm.utils.AppLog
import com.lib.MTC.struct.WifiAP
import com.lib.MTC.struct.WifiDeviceList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/28/2022.
 */
@HiltViewModel
class WifiListManagerViewModel @Inject constructor(val wifiListRepository: WifiListRepository):ViewModel() {
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<UserManagerItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<UserManagerItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(wifiListRepository.getWifiList())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(ssidList: List<String>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        ssidList.forEach {
            viewData.add(WifiListItemViewModel(it))
        }
        return viewData
    }
}