package com.example.advmvvm.ui.videoEncode

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoEncodeActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_video_encode
    }
    
    override fun initializeComponents() {}
}