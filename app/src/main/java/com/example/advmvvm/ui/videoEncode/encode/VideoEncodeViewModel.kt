package com.example.advmvvm.ui.videoEncode.encode

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.videoEncode.encode.adapter.VideoEncodeItemViewModel
import com.lib.MTC.struct.Encode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/23/2022.
 */
@HiltViewModel
class VideoEncodeViewModel @Inject constructor(
    private val videoEncodeRepository: VideoEncodeRepository
) : ViewModel() {
    val data: LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events: LiveData<Event<VideoEncodeViewEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<VideoEncodeViewEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(videoEncodeRepository.getVideoEncodes())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listEncode: List<Encode>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listEncode.forEach {
            viewData.add(VideoEncodeItemViewModel(it))
        }
        return viewData
    }
    
    open fun onApplyClicked() {
        val encodeNewConfig = (_data.value?.get(0) as VideoEncodeItemViewModel).encode
        val res = videoEncodeRepository.setVideoEncodes(encodeNewConfig)
        onApplyVideoEncodeConfigDone(res)
        Log.d("VideoEncodeViewModel", "onApplyClicked res " + res)
    }
    
    private fun onApplyVideoEncodeConfigDone(res: Int) {
        _events.postValue(Event(VideoEncodeViewEvent.ApplyVideoEncodeConfig(res)))
    }
}

sealed class VideoEncodeViewEvent {
    data class ApplyVideoEncodeConfig(val result: Int) : VideoEncodeViewEvent()
}