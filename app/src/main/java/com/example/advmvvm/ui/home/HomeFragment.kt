package com.example.advmvvm.ui.home


import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentHomeBinding
import com.example.advmvvm.ui.home.adapter.HomePagingAdapter
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.processNextEventInCurrentThread

@AndroidEntryPoint
class HomeFragment : DataBindingBaseFragment<FragmentHomeBinding,BaseViewModel>(BaseViewModel::class.java)
    ,ItemClickListener{
    
    private val mViewModel : HomeViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_home
    }

    override fun initializeController() {
        binding.itemClickListener = this
        binding.homeViewModel = mViewModel
        
        mViewModel.events.observe(this) { event ->
            handleAction(event.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(homeItemEvent: HomeItemEvent?) {
        when(homeItemEvent){
            is HomeItemEvent.SwitchPage -> {
                when(homeItemEvent.idItem){
                    0 ->{
                        AppLog.d(TAG," --> User")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_userActivity,null)
                    }
                    1 ->{
                        AppLog.d(TAG," --> Video encode")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_videoEncodeActivity,null)
                    }
                    2 ->{
                        AppLog.d(TAG," --> Video color")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_videoColorActivity,null)
                    }
                    3 ->{
                        AppLog.d(TAG," --> Network")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_networkActivity,null)
                    }
                    4 ->{
                        AppLog.d(TAG," --> Video widget")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_videoWidgetActivity,null)
                    }
                    5 ->{
                        AppLog.d(TAG," --> Camera parameter")
                        Navigation.findNavController(binding.rvHome).navigate(R.id.action_homeFragment_to_cameraParamActivity,null)
                    }
                }
            }
        }
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
           /* R.id.btn_user -> {
                Navigation.findNavController(binding.btnUser).navigate(R.id.action_homeFragment_to_registrationActivity,null, getNavOptions())
            }*/
        }
    }
}