package com.example.advmvvm.ui.network

import androidx.appcompat.app.AppCompatActivity
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NetworkActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_network
    }
    
    override fun initializeComponents() {
    }
    
}