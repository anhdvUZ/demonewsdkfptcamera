package com.example.advmvvm.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.R
import com.example.advmvvm.base.BindingViewHolder
import com.example.advmvvm.databinding.ItemHomeBinding
import com.example.advmvvm.model.HomeObj

/**
 * Created by anhdv31 on 2/16/2022.
 */
class HomePagingAdapter : PagingDataAdapter<HomeObj,RecyclerView.ViewHolder>(REPO_COMPARATOR){
    
    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<HomeObj>() {
            override fun areItemsTheSame(oldItem: HomeObj, newItem: HomeObj) =
                oldItem.title == newItem.title
            
            override fun areContentsTheSame(oldItem: HomeObj, newItem: HomeObj) =
                oldItem.title == newItem.title
        }
    }
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        return HomeViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? HomeViewHolder)?.bind(getItem(position))
    }
    
    
    class HomeViewHolder(view : View): RecyclerView.ViewHolder(view) {
        companion object{
            fun getInstance(parent: ViewGroup):HomeViewHolder{
                val inflater = LayoutInflater.from(parent.context)
                val view = inflater.inflate(R.layout.item_home,parent,false)
                return HomeViewHolder(view)
            }
        }
        
        fun bind (homeObj : HomeObj?){
        
        }
    }
    
}