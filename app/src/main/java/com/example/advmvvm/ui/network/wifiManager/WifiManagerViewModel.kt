package com.example.advmvvm.ui.network.wifiManager

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonActiveItemViewModel
import com.example.advmvvm.ui.network.wifiManager.adapter.NetWifiManagerItemViewModel
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.ui.videoEncode.encode.adapter.VideoEncodeItemViewModel
import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.struct.NetCommon
import com.lib.MTC.struct.NetWifi
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/28/2022.
 */
@HiltViewModel
class WifiManagerViewModel @Inject constructor(
    private val wifiManagerRepository: WifiManagerRepository
):ViewModel(){
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<WifiManagerEvents>>
        get() = _events
    private val _events = MutableLiveData<Event<WifiManagerEvents>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val wifiManagers = wifiManagerRepository.getWifiManagerList()
            AppLog.d("WifiManagerViewModel","ANHDV loadData() --> " + Gson().toJson(wifiManagers) + " -- " + wifiManagers.size)
            val viewData = createViewModel(wifiManagers)
            _data.postValue(viewData)
        }
    }
    private fun createViewModel(listEncode: List<NetWifi>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listEncode.forEach {
            viewData.add(NetWifiManagerItemViewModel(it))
        }
        return viewData
    }
    
    open fun onApplyClicked(){
        val netWifiNewConfig = (_data.value?.get(0) as NetWifiManagerItemViewModel).netWifi
        val res = wifiManagerRepository.setWifiManager(netWifiNewConfig)
        onApplyWifiManagerConfig(res)
        Log.d("WifiManagerViewModel","onApplyClicked res " + res)
    }
    
    fun onApplyWifiManagerConfig(res : Int){
        _events.postValue(Event(WifiManagerEvents.ApplyWifiManagerConfig(res)))
    }
}

sealed class WifiManagerEvents{
    data class ApplyWifiManagerConfig(val result: Int) : WifiManagerEvents(){}
}