package com.example.advmvvm.ui.videoEncode.encodeV2

import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.preference.*
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.PreferenceBaseFragment
import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.struct.Encode
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoEncodeFragmentV2 : PreferenceBaseFragment<BaseViewModel>(BaseViewModel::class.java), Preference.OnPreferenceChangeListener,
    SharedPreferences.OnSharedPreferenceChangeListener {
    
    private val mViewModel: VideoEncodeViewModelV2 by viewModels()
    
    override fun getXmlRes() = R.xml.video_encode
    
    private var sharedPreferences: SharedPreferences? = null
    
    private var qualitySPC0: String? = null
    private var fpsSPC0: String? = null
    private var bitrateSPC0: String? = null
    private var gopSPC0: String? = null
    
    private var qualitySPC1: String? = null
    private var fpsSPC1: String? = null
    private var bitrateSPC1: String? = null
    private var gopSPC1: String? = null
    
    private var videoEnableC0: Boolean? = null
    private var audioEnableC0: Boolean? = null
    
    private var videoEnableC1: Boolean? = null
    private var audioEnableC1: Boolean? = null
    
    private var compressionsValC0: String? = null
    private var resolutionsValC0: String? = null
    private var bitctrlsValC0: String? = null
    
    private var compressionsValC1: String? = null
    private var resolutionsValC1: String? = null
    private var bitctrlsValC1: String? = null
    
    private var sharedEdit: SharedPreferences.Editor? = null
    private var originEncode: Encode? = null
    
    private var audioPreferenceC0: SwitchPreferenceCompat? = null
    private var videoPreferenceC0: SwitchPreferenceCompat? = null
    
    private var compressionPreferenceC0: ListPreference? = null
    private var resolutionPreferenceC0: ListPreference? = null
    private var bitratectrlPreferenceC0: ListPreference? = null
    
    private var qualityPreferenceC0: EditTextPreference? = null
    private var fpsPreferenceC0: EditTextPreference? = null
    private var bitratePreferenceC0: EditTextPreference? = null
    private var gopPreferenceC0: EditTextPreference? = null
    
    private var audioPreferenceC1: SwitchPreferenceCompat? = null
    private var videoPreferenceC1: SwitchPreferenceCompat? = null
    
    private var compressionPreferenceC1: ListPreference? = null
    private var resolutionPreferenceC1: ListPreference? = null
    private var bitratectrlPreferenceC1: ListPreference? = null
    
    private var qualityPreferenceC1: EditTextPreference? = null
    private var fpsPreferenceC1: EditTextPreference? = null
    private var bitratePreferenceC1: EditTextPreference? = null
    private var gopPreferenceC1: EditTextPreference? = null
    
    override fun initializeController() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        sharedEdit = sharedPreferences?.edit()
        
        qualitySPC0 = sharedPreferences?.getString("channel_0_quality_value", "")
        fpsSPC0 = sharedPreferences?.getString("channel_0_fps_value", "")
        bitrateSPC0 = sharedPreferences?.getString("channel_0_bitrate_value", "")
        gopSPC0 = sharedPreferences?.getString("channel_0_gop_value", "")
        
        
        qualitySPC1 = sharedPreferences?.getString("channel_1_quality_value", "")
        fpsSPC1 = sharedPreferences?.getString("channel_1_fps_value", "")
        bitrateSPC1 = sharedPreferences?.getString("channel_1_bitrate_value", "")
        gopSPC1 = sharedPreferences?.getString("channel_1_gop_value", "")
        
        compressionsValC0 = sharedPreferences?.getString("channel_0_compression_value", "")
        resolutionsValC0 = sharedPreferences?.getString("channel_0_resolution_value", "")
        bitctrlsValC0 = sharedPreferences?.getString("channel_0_bitratectrl_value", "")
        
        compressionsValC1 = sharedPreferences?.getString("channel_1_compression_value", "")
        resolutionsValC1 = sharedPreferences?.getString("channel_1_resolution_value", "")
        bitctrlsValC1 = sharedPreferences?.getString("channel_1_bitratectrl_value", "")
        
        videoEnableC0 = sharedPreferences?.getBoolean("channel_0_video_enable", false)
        audioEnableC0 = sharedPreferences?.getBoolean("channel_0_audio_enable", false)
        
        videoEnableC1 = sharedPreferences?.getBoolean("channel_1_video_enable", false)
        audioEnableC1 = sharedPreferences?.getBoolean("channel_1_audio_enable", false)
        
        mViewModel.encodes.observe(this) {
            //AppLog.d(TAG,"ANHDV get video encode value " + Gson().toJson(it))
            if (it.isNotEmpty()) {
                AppLog.d(
                    TAG,
                    "ANHDV observe video encode value " + (it.first().st_1_extrafmt?.st_2_bAudioEnable == 1) + " -- " + (it.first().st_1_extrafmt?.st_1_bVideoEnable == 1)
                )
                originEncode = it.first()
                
                audioPreferenceC0?.isChecked = it.first().st_0_mainfmt?.st_2_bAudioEnable == 1
                videoPreferenceC0?.isChecked = it.first().st_0_mainfmt?.st_1_bVideoEnable == 1
                compressionPreferenceC0?.value = it.first().st_0_mainfmt?.st_0_format?.st_0_compression.toString()
                compressionPreferenceC0?.summary = compressionPreferenceC0?.value + " -- " + compressionPreferenceC0?.entry
                
                resolutionPreferenceC0?.value = it.first().st_0_mainfmt?.st_0_format?.st_1_resolution.toString()
                resolutionPreferenceC0?.summary = resolutionPreferenceC0?.value + " -- " + resolutionPreferenceC0?.entry
                
                bitratectrlPreferenceC0?.value = it.first().st_0_mainfmt?.st_0_format?.st_2_bitRateControl.toString()
                bitratectrlPreferenceC0?.summary = bitratectrlPreferenceC0?.value + " -- " + bitratectrlPreferenceC0?.entry
                
                fpsPreferenceC0?.summary = String.format(
                    getString(R.string.text_fps_value),
                    it.first().st_0_mainfmt?.st_0_format?.st_4_FPS.toString()
                )
                qualityPreferenceC0?.summary = String.format(
                    getString(R.string.text_quality_value),
                    it.first().st_0_mainfmt?.st_0_format?.st_3_quality.toString()
                )
                bitratePreferenceC0?.summary = String.format(
                    getString(R.string.text_bitrate_value),
                    it.first().st_0_mainfmt?.st_0_format?.st_5_bitRate
                )
                gopPreferenceC0?.summary = String.format(
                    getString(R.string.text_gop_value),
                    it.first().st_0_mainfmt?.st_0_format?.st_6_GOP
                )
                
                ////////////
                audioPreferenceC1?.isChecked = it.first().st_1_extrafmt?.st_2_bAudioEnable == 1
                videoPreferenceC1?.isChecked = it.first().st_1_extrafmt?.st_1_bVideoEnable == 1
                
                compressionPreferenceC1?.value = it.first().st_1_extrafmt?.st_0_format?.st_0_compression.toString()
                compressionPreferenceC1?.summary = compressionPreferenceC1?.value + " -- " + compressionPreferenceC1?.entry
                
                resolutionPreferenceC1?.value = it.first().st_1_extrafmt?.st_0_format?.st_1_resolution.toString()
                resolutionPreferenceC1?.summary = resolutionPreferenceC1?.value + " -- " + resolutionPreferenceC1?.entry
                
                bitratectrlPreferenceC1?.value = it.first().st_1_extrafmt?.st_0_format?.st_2_bitRateControl.toString()
                bitratectrlPreferenceC1?.summary = bitratectrlPreferenceC1?.value + " -- " + bitratectrlPreferenceC1?.entry
                
                
                fpsPreferenceC1?.summary = String.format(
                    getString(R.string.text_fps_value),
                    it.first().st_1_extrafmt?.st_0_format?.st_4_FPS.toString()
                )
                qualityPreferenceC1?.summary = String.format(
                    getString(R.string.text_quality_value),
                    it.first().st_1_extrafmt?.st_0_format?.st_3_quality.toString()
                )
                bitratePreferenceC1?.summary = String.format(
                    getString(R.string.text_bitrate_value),
                    it.first().st_1_extrafmt?.st_0_format?.st_5_bitRate
                )
                gopPreferenceC1?.summary = String.format(
                    getString(R.string.text_gop_value),
                    it.first().st_1_extrafmt?.st_0_format?.st_6_GOP
                )
                
                sharedEdit?.putBoolean("channel_0_audio_enable", it.first().st_0_mainfmt?.st_2_bAudioEnable == 1)
                sharedEdit?.putBoolean("channel_0_video_enable", it.first().st_0_mainfmt?.st_1_bVideoEnable == 1)
                
                sharedEdit?.putString("channel_0_compression_value", it.first().st_0_mainfmt?.st_0_format?.st_0_compression.toString())
                sharedEdit?.putString("channel_0_resolution_value", it.first().st_0_mainfmt?.st_0_format?.st_1_resolution.toString())
                sharedEdit?.putString("channel_0_bitratectrl_value", it.first().st_0_mainfmt?.st_0_format?.st_2_bitRateControl.toString())
                
                sharedEdit?.putString("channel_0_quality_value", it.first().st_0_mainfmt?.st_0_format?.st_3_quality.toString())
                sharedEdit?.putString("channel_0_fps_value", it.first().st_0_mainfmt?.st_0_format?.st_4_FPS.toString())
                sharedEdit?.putString("channel_0_bitrate_value", it.first().st_0_mainfmt?.st_0_format?.st_5_bitRate.toString())
                sharedEdit?.putString("channel_0_gop_value", it.first().st_0_mainfmt?.st_0_format?.st_6_GOP.toString())
                
                ///////////////////////////
                sharedEdit?.putBoolean("channel_1_audio_enable", it.first().st_1_extrafmt?.st_2_bAudioEnable == 1)
                sharedEdit?.putBoolean("channel_1_video_enable", it.first().st_1_extrafmt?.st_1_bVideoEnable == 1)
                
                sharedEdit?.putString("channel_1_compression_value", it.first().st_1_extrafmt?.st_0_format?.st_0_compression.toString())
                sharedEdit?.putString("channel_1_resolution_value", it.first().st_1_extrafmt?.st_0_format?.st_1_resolution.toString())
                sharedEdit?.putString("channel_1_bitratectrl_value", it.first().st_1_extrafmt?.st_0_format?.st_2_bitRateControl.toString())
                
                sharedEdit?.putString("channel_1_quality_value", it.first().st_1_extrafmt?.st_0_format?.st_3_quality.toString())
                sharedEdit?.putString("channel_1_fps_value", it.first().st_1_extrafmt?.st_0_format?.st_4_FPS.toString())
                sharedEdit?.putString("channel_1_bitrate_value", it.first().st_1_extrafmt?.st_0_format?.st_5_bitRate.toString())
                sharedEdit?.putString("channel_1_gop_value", it.first().st_1_extrafmt?.st_0_format?.st_6_GOP.toString())
                
                sharedEdit?.commit()
            } else {
                Toast.makeText(requireContext(), "Server error !! No data", Toast.LENGTH_LONG).show()
            }
        }
    }
    
    override fun initializePreferences() {
        audioPreferenceC0 = findPreference("channel_0_audio_enable")
        videoPreferenceC0 = findPreference("channel_0_video_enable")
        
        compressionPreferenceC0 = findPreference("channel_0_compression_value")
        resolutionPreferenceC0 = findPreference("channel_0_resolution_value")
        bitratectrlPreferenceC0 = findPreference("channel_0_bitratectrl_value")
        
        qualityPreferenceC0 = findPreference("channel_0_quality_value")
        fpsPreferenceC0 = findPreference("channel_0_fps_value")
        bitratePreferenceC0 = findPreference("channel_0_bitrate_value")
        gopPreferenceC0 = findPreference("channel_0_gop_value")
        
        audioPreferenceC1 = findPreference("channel_1_audio_enable")
        videoPreferenceC1 = findPreference("channel_1_video_enable")
        
        compressionPreferenceC1 = findPreference("channel_1_compression_value")
        resolutionPreferenceC1 = findPreference("channel_1_resolution_value")
        bitratectrlPreferenceC1 = findPreference("channel_1_bitratectrl_value")
        
        qualityPreferenceC1 = findPreference("channel_1_quality_value")
        fpsPreferenceC1 = findPreference("channel_1_fps_value")
        bitratePreferenceC1 = findPreference("channel_1_bitrate_value")
        gopPreferenceC1 = findPreference("channel_1_gop_value")
        
        audioPreferenceC0?.onPreferenceChangeListener = this
        videoPreferenceC0?.onPreferenceChangeListener = this
        qualityPreferenceC0?.onPreferenceChangeListener = this
        fpsPreferenceC0?.onPreferenceChangeListener = this
        bitratePreferenceC0?.onPreferenceChangeListener = this
        gopPreferenceC0?.onPreferenceChangeListener = this
        compressionPreferenceC0?.onPreferenceChangeListener = this
        resolutionPreferenceC0?.onPreferenceChangeListener = this
        bitratectrlPreferenceC0?.onPreferenceChangeListener = this
        //////////////////////
        audioPreferenceC1?.onPreferenceChangeListener = this
        videoPreferenceC1?.onPreferenceChangeListener = this
        qualityPreferenceC1?.onPreferenceChangeListener = this
        fpsPreferenceC1?.onPreferenceChangeListener = this
        bitratePreferenceC1?.onPreferenceChangeListener = this
        gopPreferenceC1?.onPreferenceChangeListener = this
        compressionPreferenceC1?.onPreferenceChangeListener = this
        resolutionPreferenceC1?.onPreferenceChangeListener = this
        bitratectrlPreferenceC1?.onPreferenceChangeListener = this
    }
    
    override fun onPreferenceChange(preference: Preference, newValue: Any?): Boolean {
        AppLog.d(TAG, "ANHDV onPreferenceChange 0" + preference.key + " -- " + Gson().toJson(newValue))
        when (preference.key) {
            "channel_0_audio_enable" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_0_mainfmt?.st_2_bAudioEnable =  newValue?.toString()?.compareTo("true")
            }
            "channel_0_video_enable" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_0_mainfmt?.st_1_bVideoEnable =  newValue?.toString()?.compareTo("true")
            }
            "channel_0_compression_value" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_0_mainfmt?.st_0_format?.st_0_compression = newValue?.toString()?.toInt()
            }
            "channel_0_resolution_value" -> {
                //sharedEdit?.putString("resolution_value",)
                AppLog.d(TAG, "ANHDV onPreferenceChange 2 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_0_mainfmt?.st_0_format?.st_1_resolution = newValue?.toString()?.toInt()
            }
            "channel_0_bitratectrl_value" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 3 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_0_mainfmt?.st_0_format?.st_2_bitRateControl = newValue?.toString()?.toInt()
            }
            "channel_0_quality_value" -> {
                originEncode?.st_0_mainfmt?.st_0_format?.st_3_quality = newValue?.toString()?.toInt()
            }
            "channel_0_fps_value" -> {
                originEncode?.st_0_mainfmt?.st_0_format?.st_4_FPS = newValue?.toString()?.toInt()
            }
            "channel_0_bitrate_value" -> {
                originEncode?.st_0_mainfmt?.st_0_format?.st_5_bitRate = newValue?.toString()?.toInt()
            }
            "channel_0_gop_value" -> {
                originEncode?.st_0_mainfmt?.st_0_format?.st_6_GOP = newValue?.toString()?.toInt()
                
            }
            ///
            "channel_1_audio_enable" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_1_extrafmt?.st_2_bAudioEnable = newValue?.toString()?.compareTo("true")
            }
            "channel_1_video_enable" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_1_extrafmt?.st_1_bVideoEnable = newValue?.toString()?.compareTo("true")
            }
            "channel_1_compression_value" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 1 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_1_extrafmt?.st_0_format?.st_0_compression = newValue?.toString()?.toInt()
            }
            "channel_1_resolution_value" -> {
                //sharedEdit?.putString("resolution_value",)
                AppLog.d(TAG, "ANHDV onPreferenceChange 2 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_1_extrafmt?.st_0_format?.st_1_resolution = newValue?.toString()?.toInt()
            }
            "channel_1_bitratectrl_value" -> {
                AppLog.d(TAG, "ANHDV onPreferenceChange 3 " + preference.key + " -- " + Gson().toJson(newValue))
                originEncode?.st_1_extrafmt?.st_0_format?.st_2_bitRateControl = newValue?.toString()?.toInt()
            }
            "channel_1_quality_value" -> {
                originEncode?.st_1_extrafmt?.st_0_format?.st_3_quality = newValue?.toString()?.toInt()
            }
            "channel_1_fps_value" -> {
                originEncode?.st_1_extrafmt?.st_0_format?.st_4_FPS = newValue?.toString()?.toInt()
            }
            "channel_1_bitrate_value" -> {
                originEncode?.st_1_extrafmt?.st_0_format?.st_5_bitRate = newValue?.toString()?.toInt()
            }
            "channel_1_gop_value" -> {
                originEncode?.st_1_extrafmt?.st_0_format?.st_6_GOP = newValue?.toString()?.toInt()
                
            }
        }
        mViewModel.setData(originEncode)
        return true
    }
    
    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        AppLog.d(TAG, "ANHDV onSharedPreferenceChanged " + p1)
        //fpsPreferenceC0?.setSummary(String.format(getString(R.string.text_fps_value),sharedPreferences?.getString("channel_0_fps_value","")))
        preferenceScreen = null
        addPreferencesFromResource(getXmlRes())
    }
    
}