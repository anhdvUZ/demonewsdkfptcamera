package com.example.advmvvm.ui.user

import android.view.View
import androidx.navigation.Navigation
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentUserBinding
import com.example.advmvvm.utils.AppLog

class UserFragment : DataBindingBaseFragment<FragmentUserBinding, BaseViewModel>(BaseViewModel::class.java), ItemClickListener {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_user
    }
    
    override fun initializeController() {
        binding.itemClickListener = this
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.tv_user_manager->{
                AppLog.d(TAG,"User manager")
                Navigation.findNavController(binding.tvUserManager).navigate(R.id.action_userFragment_to_userFragmentManager,null,getNavOptions())
            }
            R.id.tv_modify_password->{
                AppLog.d(TAG,"Modify password")
                Navigation.findNavController(binding.tvModifyPassword).navigate(R.id.action_userFragment_to_modifyPasswordFragment,null,getNavOptions())
            }
        }
    }
}