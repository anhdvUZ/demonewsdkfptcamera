package com.example.advmvvm.ui.videoEncode.encodeV2

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode

/**
 * Created by anhdv31 on 8/1/2022.
 */
class VideoEncodeRepositoryV2 {
    fun getVideoEncodes(): List<Encode> {
        val encodes = arrayListOf<Encode>()
        val encode = Encode()
        val byteObj = Futils.ObjToBytes(encode)
        val resGetConfig = MTC.DEV_GetConfigEncode(MTC.loginID, byteObj)
        AppLog.d("VideoEncodeRepositoryV2", "ANHDV DEV_GetConfigEncode --> loginId " + MTC.loginID + " -- " + resGetConfig)
        if (resGetConfig >= 0) {
            val retGetEncodeInfo = Futils.BytesToObj(encode, byteObj)
            AppLog.d("VideoEncodeRepositoryV2", "ANHDV --> " + Gson().toJson(encode))
            encodes.add(encode)
        }
        return encodes
    }
    
    fun setVideoEncodes(encode: Encode?): Int {
        val byteObj = Futils.ObjToBytes(encode)
        AppLog.d("VideoEncodeRepositoryV2", "ANHDV setVideoEncodes --> " + Gson().toJson(encode))
        return MTC.DEV_SetConfigEncode(MTC.loginID, byteObj)
    }
}