package com.example.advmvvm.ui.network.wifiManager

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentWifiManagerBinding
import com.example.advmvvm.ui.network.netCommonActive.NetCommonActiveViewModel
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WifiManagerFragment : DataBindingBaseFragment<FragmentWifiManagerBinding,BaseViewModel>(BaseViewModel::class.java) {
    
    private val mViewModel : WifiManagerViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_wifi_manager
    }
    
    override fun initializeController() {
        binding.wifiManagerViewmodel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(wifiManagerEvents: WifiManagerEvents?) {
        when(wifiManagerEvents){
            is WifiManagerEvents.ApplyWifiManagerConfig -> {
                val res = wifiManagerEvents.result
                AppLog.d(TAG,"WifiManagerEvents " + res)
                Toast.makeText(context,"WifiManagerEvents " + res, Toast.LENGTH_LONG).show()
            }
        }
    }
    
}