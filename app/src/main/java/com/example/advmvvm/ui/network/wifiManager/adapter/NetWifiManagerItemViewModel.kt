package com.example.advmvvm.ui.network.wifiManager.adapter

import android.util.Log
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.NetWifi
import java.lang.Exception

/**
 * Created by anhdv31 on 2/28/2022.
 */
class NetWifiManagerItemViewModel (var netWifi: NetWifi):BaseItemViewModel{
    override val layoutId: Int
        get() = R.layout.item_net_wifi_manager
    fun getNetWifiManagerStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(netWifi)
    
    fun onWifiManagerConfigChange(config: CharSequence) {
        Log.d("NetWifiManagerItemViewModel", "ANHDV onWifiManagerConfigChange" + config)
        val gson = Gson()
        try {
            netWifi = gson.fromJson(config.toString(), NetWifi::class.java)
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    
}