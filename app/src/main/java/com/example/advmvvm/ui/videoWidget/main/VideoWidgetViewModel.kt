package com.example.advmvvm.ui.videoWidget.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.videoWidget.adapter.VideoWidgetItemViewModel
import com.lib.MTC.struct.VideoWidget
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 3/1/2022.
 */
@HiltViewModel
class VideoWidgetViewModel @Inject constructor(val videoWidgetRepository: VideoWidgetRepository)
    : ViewModel() {
    val data: LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events: LiveData<Event<VideoWidgetViewEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<VideoWidgetViewEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(videoWidgetRepository.getVideoWidgets())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listVideoWidget: List<VideoWidget>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listVideoWidget.forEach {
            viewData.add(VideoWidgetItemViewModel(it))
        }
        return viewData
    }
    
    open fun onApplyClicked() {
        val videoWidgetNewConfig = (_data.value?.get(0) as VideoWidgetItemViewModel).videoWidget
        val res = videoWidgetRepository.setVideoWidgets(videoWidgetNewConfig)
        onApplyVideoWidgetConfigDone(res)
        Log.d("VideoWidgetViewModel", "onApplyClicked res " + res)
    }
    
    private fun onApplyVideoWidgetConfigDone(res: Int) {
        _events.postValue(Event(VideoWidgetViewEvent.ApplyVideoWidgetConfig(res)))
    }
    
}

sealed class VideoWidgetViewEvent {
    data class ApplyVideoWidgetConfig(val result: Int) : VideoWidgetViewEvent()
}