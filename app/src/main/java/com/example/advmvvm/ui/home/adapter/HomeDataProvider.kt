package com.example.advmvvm.ui.home.adapter

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by anhdv31 on 2/21/2022.
 */

data class HomeData(
    val id : Int,
    val title : String,
    val description : String,
    val icon : Int
)

class HomeDataProvider {
    val gson = Gson()
    
    fun getHomeListData():List<HomeData>{
        val typeToken = object: TypeToken<List<HomeData>>(){}.type
        return gson.fromJson(getStringHomeListData(),typeToken)
    }
    
    private fun getStringHomeListData() : String {
        return "[{id:0,title:\"User\",description:\"\",icon:0},{id:1,title:\"Video encode\",description:\"\",icon:0},{id:2,title:\"Video color\",description:\"\",icon:0},{id:3,title:\"Network\",description:\"\",icon:0},{id:4,title:\"Video widget\",description:\"\",icon:0},{id:5,title:\"Camera parameter\",description:\"\",icon:0}]".trimIndent()
    }
}

