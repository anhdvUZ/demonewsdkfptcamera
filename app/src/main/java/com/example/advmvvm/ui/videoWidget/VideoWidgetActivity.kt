package com.example.advmvvm.ui.videoWidget

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoWidgetActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_video_widget
    }
    
    override fun initializeComponents() {
    
    }
}