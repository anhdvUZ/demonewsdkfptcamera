package com.example.advmvvm.ui.network.netCommonActive

import androidx.lifecycle.*
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonActiveItemViewModel
import com.example.advmvvm.ui.videoEncode.encode.adapter.VideoEncodeItemViewModel
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.NetCommon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/23/2022.
 */
@HiltViewModel
class NetCommonActiveViewModel @Inject constructor(
    private val netCommonActiveRepository: NetCommonActiveRepository
):ViewModel() {
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val event : LiveData<Event<NetCommonActiveEvents>>
    get()  = _event
    private val _event = MutableLiveData<Event<NetCommonActiveEvents>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(netCommonActiveRepository.getNetCommonActives())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listEncode: List<NetCommon>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        listEncode.forEach {
            viewData.add(NetCommonActiveItemViewModel(it))
        }
        return viewData
    }
    
    fun onApplyClicked(){
        val netCommonActiveNewConfig = (_data.value?.get(0) as NetCommonActiveItemViewModel).netCommon
        val res = netCommonActiveRepository.setNetCommonActives(netCommonActiveNewConfig)
        onApplyNetCommonActiveDone(res)
    }
    
    private fun onApplyNetCommonActiveDone(res : Int){
        _event.postValue(Event(NetCommonActiveEvents.ApplyNetCommonActive(res)))
    }
}

sealed class NetCommonActiveEvents{
    data class ApplyNetCommonActive(val result: Int) : NetCommonActiveEvents()
}