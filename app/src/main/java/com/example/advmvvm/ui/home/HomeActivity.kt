package com.example.advmvvm.ui.home


import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity(){
    override fun getLayoutRes(): Int {
        return R.layout.activity_home
    }

    override fun initializeComponents() {}
}