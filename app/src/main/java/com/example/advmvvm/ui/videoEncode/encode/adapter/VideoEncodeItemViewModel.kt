package com.example.advmvvm.ui.videoEncode.encode.adapter

import android.util.Log
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.Encode
import java.lang.Exception

/**
 * Created by anhdv31 on 2/23/2022.
 */
class VideoEncodeItemViewModel(
    var encode: Encode
) : BaseItemViewModel {
    override val layoutId: Int = R.layout.item_video_encode
    
    fun getData() = encode
    
    fun getVideoEncodeString() = Gson().toJson(encode)
    
    fun getVideoEncodeStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(encode)
    
    fun onVideoEncodeConfigChange(config: CharSequence) {
        Log.d("VideoEncode", "ANHDV onVideoEncodeConfigChange" + config)
        val gson = Gson()
        try {
            encode = gson.fromJson(config.toString(), Encode::class.java)
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}