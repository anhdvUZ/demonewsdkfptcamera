package com.example.advmvvm.ui.network.wifiList

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.WifiDeviceList

/**
 * Created by anhdv31 on 2/28/2022.
 */
class WifiListRepository {
    val gson = Gson()
    fun getWifiList(): List<String> {
        var ssidList = arrayListOf<String>()
        //val byteArray = Futils.ObjToBytes(jsonString)
        val wifiDeviceList = WifiDeviceList()
        val array = Futils.ObjToBytes(wifiDeviceList)
        val param = ByteArray(array.size)
        val res = MTC.DEV_GetNetWifiList(MTC.loginID, param)
        AppLog.d("WifiListRepository","ANHDV getWifiList --> " + res)
        Futils.BytesToObj(wifiDeviceList, param)
        for (i in 0..wifiDeviceList.st_0_number){
            val ssid = Futils.ByteToString(wifiDeviceList.st_1_wifiDeviceAll[i].st_0_ssid)
            ssidList.add(if (ssid.isEmpty()) "Noname"  else ssid)
        }
        return ssidList
    }
}