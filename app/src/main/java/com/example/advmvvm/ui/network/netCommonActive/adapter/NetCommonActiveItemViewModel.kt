package com.example.advmvvm.ui.network.netCommonActive.adapter

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.BaseViewModel
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.NetCommon

/**
 * Created by anhdv31 on 2/23/2022.
 */
class NetCommonActiveItemViewModel(val netCommon: NetCommon):BaseItemViewModel {
    override val layoutId: Int
        get() = R.layout.item_net_common_active
    fun getNetCommonActiveStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(netCommon)
}