package com.example.advmvvm.ui.videoColor.main

import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentVideoColorBinding
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoColorFragment : DataBindingBaseFragment<FragmentVideoColorBinding, BaseViewModel>(BaseViewModel::class.java) {
    
    private val mViewModel: VideoColorViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_video_color
    }
    
    override fun initializeController() {
        binding.videoColorViewModel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(videoColorEvent: VideoColorEvent?) {
        when(videoColorEvent){
            is VideoColorEvent.ApplyVideoColorConfig -> {
                val res = videoColorEvent.result
                AppLog.d(TAG,"VideoColorEvent " + res)
                Toast.makeText(context,"VideoColorConfig  " + res, Toast.LENGTH_LONG).show()
            }
        }
    }
    
}