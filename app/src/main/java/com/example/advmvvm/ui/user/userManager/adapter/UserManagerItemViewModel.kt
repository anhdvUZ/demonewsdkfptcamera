package com.example.advmvvm.ui.user.userManager.adapter

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 2/22/2022.
 */
class UserManagerItemViewModel (
    val uname : String,
    val groupName : String,
    val onItemClick : (String) -> Unit
):BaseItemViewModel{
    override val layoutId: Int = R.layout.item_user_manager
    
    override val viewType: Int
        get() = super.viewType
    
    fun onClick(){
        onItemClick(uname);
    }
}