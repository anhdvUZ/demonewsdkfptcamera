package com.example.advmvvm.ui.network.networkModule

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentNetworkBinding
import com.example.advmvvm.utils.AppLog
import kotlinx.android.synthetic.main.fragment_network.*

class NetworkFragment : DataBindingBaseFragment<FragmentNetworkBinding,BaseViewModel>(BaseViewModel::class.java)
,ItemClickListener{
    override fun getLayoutRes(): Int {
        return  R.layout.fragment_network
    }
    
    override fun initializeController() {
        binding.itemClickListener = this
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.tv_net_common_active -> {
                AppLog.d(TAG,"tv_net_common_active")
                Navigation.findNavController(binding.tvNetCommonActive).navigate(R.id.action_networkFragment_to_netActiveCommonFragment,null,getNavOptions())
            }
            R.id.tv_wifi_list -> {
                AppLog.d(TAG,"tv_wifi_list")
                Navigation.findNavController(binding.tvWifiList).navigate(R.id.action_networkFragment_to_wifiListFragment,null,getNavOptions())
            }
            R.id.tv_wifi_manager -> {
                AppLog.d(TAG,"tv_wifi_manager")
                Navigation.findNavController(binding.tvWifiManager).navigate(R.id.action_networkFragment_to_wifiManagerFragment,null,getNavOptions())
            }
        }
    }
    
}