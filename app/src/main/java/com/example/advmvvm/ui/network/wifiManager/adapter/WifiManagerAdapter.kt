package com.example.advmvvm.ui.network.wifiManager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/28/2022.
 */
class WifiManagerAdapter : RecyclerView.Adapter<WifiManagerViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WifiManagerViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return WifiManagerViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: WifiManagerViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class WifiManagerViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.netWifiManagerItemViewModel, itemViewModel)
    }
}

@BindingAdapter("wifiManagerItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels: List<BaseItemViewModel>?) {
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): WifiManagerAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is WifiManagerAdapter) {
        recyclerView.adapter as WifiManagerAdapter
    } else {
        val bindableRecyclerAdapter = WifiManagerAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}