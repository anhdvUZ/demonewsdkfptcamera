package com.example.advmvvm.ui.videoWidget.adapter

import android.util.Log
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.VideoWidget
import java.lang.Exception

/**
 * Created by anhdv31 on 3/1/2022.
 */
class VideoWidgetItemViewModel (var videoWidget: VideoWidget) : BaseItemViewModel{
    
    override val layoutId: Int =  R.layout.item_video_widget
    
    fun getData() = videoWidget
    
    fun getVideoWidgetStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(videoWidget)
    
    fun onVideoWidgetConfigChange(config: CharSequence) {
        Log.d("VideoWidgetItemView", "ANHDV onVideoWidgetConfigChange" + config)
        val gson = Gson()
        try {
            videoWidget = gson.fromJson(config.toString(), VideoWidget::class.java)
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}