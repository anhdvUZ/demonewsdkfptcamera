package com.example.advmvvm.ui.network.wifiList

import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentWifiListBinding
import com.example.advmvvm.ui.cameraParam.main.CameraParamViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WifiListFragment : DataBindingBaseFragment<FragmentWifiListBinding,BaseViewModel>(BaseViewModel::class.java) {
    private val mViewModel: WifiListManagerViewModel by viewModels()
    override fun getLayoutRes(): Int {
        return R.layout.fragment_wifi_list
    }
    
    override fun initializeController() {
        binding.wifiListManagerViewModel = mViewModel
    }
}