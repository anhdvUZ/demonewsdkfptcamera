package com.example.advmvvm.ui.videoColor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoColorActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_video_color
    }
    
    override fun initializeComponents() {}
}