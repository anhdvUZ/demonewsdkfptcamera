package com.example.advmvvm.ui.network.wifiList.adapter

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/28/2022.
 */
class WifiListItemViewModel (
    val ssidName : String,
):BaseItemViewModel{
    override val layoutId: Int = R.layout.item_wifi_list
}