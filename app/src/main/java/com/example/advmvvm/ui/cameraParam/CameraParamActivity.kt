package com.example.advmvvm.ui.cameraParam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CameraParamActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_camera_param
    }
    
    override fun initializeComponents() {}
    
}