package com.example.advmvvm.ui.cameraParam.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.ui.cameraParam.main.CameraParamViewModel
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonAciveViewHolder
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonActiveAdapter

/**
 * Created by anhdv31 on 2/28/2022.
 */
class CameraParamAdapter : RecyclerView.Adapter<CameraParamViewHolder>(){
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraParamViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return CameraParamViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: CameraParamViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
}
class CameraParamViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.cameraParamItemViewModel, itemViewModel)
    }
}

@BindingAdapter("cameraParamItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels: List<BaseItemViewModel>?) {
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): CameraParamAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is CameraParamAdapter) {
        recyclerView.adapter as CameraParamAdapter
    } else {
        val bindableRecyclerAdapter = CameraParamAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}