package com.example.advmvvm.ui.splash

import android.os.Looper
import android.os.Handler
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseActivity
import com.example.advmvvm.databinding.ActivitySplashBinding
import com.example.advmvvm.ui.home.HomeActivity
import com.example.advmvvm.ui.register.RegisterActivity
import com.example.advmvvm.utils.startMyActivity

class SplashActivity : DataBindingBaseActivity<ActivitySplashBinding,BaseViewModel>(BaseViewModel::class.java)
    , Animation.AnimationListener {

    override fun getLayoutRes(): Int {
        return R.layout.activity_splash
    }

    override fun initializeComponents() {
        binding.lifecycleOwner = this
        val displayAnimation = AnimationUtils.loadAnimation(applicationContext,R.anim.transition_from_top)
        displayAnimation.setAnimationListener(this)
        //binding.imgIcon.startAnimation(displayAnimation)
    
        val delayMillis: Long = 2000
        Handler(Looper.getMainLooper()).postDelayed({ performNavigationHomeActivity() },delayMillis)
    }

    override fun onAnimationStart(p0: Animation?) {}

    override fun onAnimationEnd(p0: Animation?) {
        val delayMillis: Long = 2000
        Handler(Looper.getMainLooper()).postDelayed({ performNavigationHomeActivity() },delayMillis)
    }

    override fun onAnimationRepeat(p0: Animation?) {}

    private fun performNavigationHomeActivity(){
        //this.startMyActivity(HomeActivity::class.java)
        this.startMyActivity(RegisterActivity::class.java)
        finish()
    }

}