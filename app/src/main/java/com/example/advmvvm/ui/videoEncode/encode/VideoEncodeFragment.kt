package com.example.advmvvm.ui.videoEncode.encode

import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentVideoEncodeBinding
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoEncodeFragment : DataBindingBaseFragment<FragmentVideoEncodeBinding, BaseViewModel>(BaseViewModel::class.java) {
    
    private val mViewModel: VideoEncodeViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_video_encode
    }
    
    override fun initializeController() {
        binding.videoEncodeViewModel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(videoEncodeViewEvent: VideoEncodeViewEvent?) {
        when(videoEncodeViewEvent){
            is VideoEncodeViewEvent.ApplyVideoEncodeConfig -> {
                val res = videoEncodeViewEvent.result
                AppLog.d(TAG,"videoEncodeViewEvent " + res)
                Toast.makeText(context,"VideoEncodeConfig " + res,Toast.LENGTH_LONG).show()
            }
        }
    }
    
}