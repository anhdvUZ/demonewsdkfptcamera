package com.example.advmvvm.ui.videoWidget.main

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.VideoWidget

/**
 * Created by anhdv31 on 3/1/2022.
 */
class VideoWidgetRepository {
    
    fun getVideoWidgets():List<VideoWidget>{
        val videoWidgets = arrayListOf<VideoWidget>()
        val videoWidget = VideoWidget()
        val byteObj = Futils.ObjToBytes(videoWidget)
        val res : Int = MTC.DEV_GetVideoWidget(MTC.loginID,byteObj)
        AppLog.d("VideoWidgetRepository","ANHDV DEV_GetVideoWidget --> loginId " + MTC.loginID + " -- " + res)
        if (res >= 0){
            val retGetEncodeInfo = Futils.BytesToObj(videoWidget,byteObj)
            AppLog.d("VideoWidgetRepository","ANHDV --> " + Gson().toJson(videoWidget))
            videoWidgets.add(videoWidget)
        }
        return videoWidgets
    }
    
    fun setVideoWidgets(videoWidget: VideoWidget): Int {
        val byteObj = Futils.ObjToBytes(videoWidget)
        AppLog.d("VideoEncodeRepository","ANHDV setVideoEncodes --> " + Gson().toJson(videoWidget))
        return MTC.DEV_SetVideoWidget(MTC.loginID, byteObj)
    }
}