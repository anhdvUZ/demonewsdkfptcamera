package com.example.advmvvm.ui.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentForgotPasswordBinding

class ForgotPasswordFragment : DataBindingBaseFragment<FragmentForgotPasswordBinding,BaseViewModel>(BaseViewModel::class.java)
    ,ItemClickListener{
    override fun getLayoutRes(): Int {
        return R.layout.fragment_forgot_password
    }
    
    override fun initializeController() {
        binding.lifecycleOwner = this
    }
    
    override fun onItemClickListener(view: View) {
   
    }
}