package com.example.advmvvm.ui.network.wifiManager

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.NetWifi

/**
 * Created by anhdv31 on 2/28/2022.
 */
class WifiManagerRepository {
    fun getWifiManagerList():List<NetWifi>{
        val wifiManagers = arrayListOf<NetWifi>()
        val netWifi = NetWifi()
        AppLog.d("WifiManagerRepository","ANHDV WifiManagerRepository")
        val byteObj = Futils.ObjToBytes(netWifi)
        //val byteParam = ByteArray(byteObj.size)
        val res : Int = MTC.DEV_GetNetWifiInfo(MTC.loginID,byteObj)
        AppLog.d("WifiManagerRepository","ANHDV WifiManagerRepository --> loginId " + MTC.loginID + " -- " + res)
        AppLog.d("WifiManagerRepository","ANHDV --> " + Gson().toJson(netWifi) + " -- " + res)
        if (res >= 0){
            var resWifiInfo = Futils.BytesToObj(netWifi,byteObj)
            wifiManagers.add(netWifi)
        }
        return wifiManagers
    }
    
    fun setWifiManager(netWifi: NetWifi): Int {
        val byteObj = Futils.ObjToBytes(netWifi)
        AppLog.d("WifiManagerRepository","ANHDV setWifiManager --> " +Gson().toJson(netWifi))
        return MTC.DEV_SetNetWifi(MTC.loginID, byteObj)
    }
}