package com.example.advmvvm.ui.videoColor.main

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.VideoColorControl

/**
 * Created by anhdv31 on 2/28/2022.
 */
class VideoColorRepository {
    
    fun getVideoColors():List<VideoColorControl>{
        val videoColorControls = arrayListOf<VideoColorControl>()
        val videoColorControl = VideoColorControl()
        val byteObj = Futils.ObjToBytes(videoColorControl)
        val res : Int = MTC.DEV_GetVideoColor(MTC.loginID,byteObj)
        AppLog.d("VideoColorRepository","ANHDV DEV_GetConfigEncode --> loginId " + MTC.loginID + " -- " + res)
        if (res >= 0){
            val retGetEncodeInfo = Futils.BytesToObj(videoColorControl,byteObj)
            AppLog.d("VideoColorRepository","ANHDV --> " + Gson().toJson(videoColorControl))
            videoColorControls.add(videoColorControl)
        }
        AppLog.d("VideoColorRepository","ANHDV --> size() --> " + videoColorControls.size)
        return videoColorControls
    }
    
    fun setVideoColors(videoColorControl: VideoColorControl): Int {
        val byteObj = Futils.ObjToBytes(videoColorControl)
        AppLog.d("VideoColorRepository","ANHDV setVideoEncodes --> " + Gson().toJson(videoColorControl))
        return MTC.DEV_SetVideoColor(MTC.loginID, byteObj)
    }
}