package com.example.advmvvm.ui.cameraParam.main

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.CameraParam
import com.lib.MTC.struct.Encode
import com.lib.MTC.struct.NetCommon

/**
 * Created by anhdv31 on 2/28/2022.
 */
class CameraParamRepository {
    fun getCameraParams(): List<CameraParam> {
        val cameraParams = arrayListOf<CameraParam>()
        val cameraParam = CameraParam()
        val byteObj = Futils.ObjToBytes(cameraParam)
        val byteParam = ByteArray(byteObj.size)
        val res : Int = MTC.DEV_GetCameraParam(MTC.loginID,byteParam)
        if (res >= 0){
            val retGetEncodeInfo = Futils.BytesToObj(cameraParam,byteParam)
            cameraParams.add(cameraParam)
        }
        return cameraParams
    }
    
    fun setCameraParam(cameraParam: CameraParam): Int {
        val byteObj = Futils.ObjToBytes(cameraParam)
        return MTC.DEV_SetCameraParam(MTC.loginID, byteObj)
    }
}