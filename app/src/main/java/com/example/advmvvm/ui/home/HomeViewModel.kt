package com.example.advmvvm.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.home.adapter.HomeData
import com.example.advmvvm.ui.home.adapter.HomeDataProvider
import com.example.advmvvm.ui.home.adapter.HomeItemViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/21/2022.
 */

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeDataProvider : HomeDataProvider
): ViewModel(){
    val data : LiveData<List<BaseItemViewModel>>
    get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<HomeItemEvent>>
        get() = _events
    private val _events = MutableLiveData<Event<HomeItemEvent>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(homeDataProvider.getHomeListData())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(listHomeData : List<HomeData>) : List<BaseItemViewModel>{
        val viewData = mutableListOf<BaseItemViewModel>()
        listHomeData.forEach {
            viewData.add(HomeItemViewModel(it.id,it.title,it.description,it.icon,::onItemHomeClicked))
        }
        return viewData
    }
    
    private fun onItemHomeClicked(idItem : Int){
        _events.postValue(Event(HomeItemEvent.SwitchPage(idItem)))
    }
}

sealed class HomeItemEvent{
    data class SwitchPage(val idItem : Int) : HomeItemEvent()
}