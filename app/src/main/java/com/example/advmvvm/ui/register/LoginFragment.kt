package com.example.advmvvm.ui.register

import android.text.Editable
import android.view.View
import androidx.navigation.Navigation
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentLoginBinding
import com.example.advmvvm.utils.AppLog
import com.example.advmvvm.utils.Constants
import com.lib.MTC.MTC

class LoginFragment : DataBindingBaseFragment<FragmentLoginBinding,BaseViewModel>(BaseViewModel::class.java)
    ,ItemClickListener {
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_login;
    }

    override fun initializeController() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
        
        binding.tilCloudId.editText?.text = Editable.Factory.getInstance().newEditable(Constants.uuid)
        binding.tilUsername.editText?.text = Editable.Factory.getInstance().newEditable(Constants.uname)
        binding.tilPwd.editText?.text = Editable.Factory.getInstance().newEditable(Constants.pwd)
    
    
        /*MTC.SDK_init()
        MTC.SetDebug()*/
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.tv_forgot->{
                Navigation
                    .findNavController(binding.tvForgot)
                    .navigate(R.id.action_loginFragment_to_forgotFragment,null,getNavOptions())
            }
            R.id.btn_login->{
                val loginId = MTC.DEV_LoginCloud(binding.tilCloudId.editText?.text.toString(),
                    binding.tilUsername.editText?.text.toString(),
                    binding.tilPwd.editText?.text.toString())
                
                AppLog.d(TAG,"ANHDV Login -->  info ->> " + binding.tilCloudId.editText?.text.toString()
                        + " -- " + binding.tilUsername.editText?.text.toString()
                        + " -- " + binding.tilPwd.editText?.text.toString())
                AppLog.d(TAG,"ANHDV --> " + loginId)
                
                if (loginId >= 0 ){
                    MTC.loginID = loginId
                    Navigation
                        .findNavController(binding.btnLogin)
                        .navigate(R.id.action_loginFragment_to_homeActivity,null)
                }
            }
        }
    }
    
}