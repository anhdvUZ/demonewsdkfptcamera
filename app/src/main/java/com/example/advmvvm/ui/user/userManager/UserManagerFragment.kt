package com.example.advmvvm.ui.user.userManager

import android.os.UserManager
import android.view.View
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.advmvvm.databinding.FragmentUserManagerBinding
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserManagerFragment : DataBindingBaseFragment<FragmentUserManagerBinding, BaseViewModel>(BaseViewModel::class.java)
,ItemClickListener {
    
    private val mViewModel : UserManagerViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_user_manager
    }
    
    override fun initializeController() {
        binding.itemClickListener = this
        binding.userManagerViewModel = mViewModel
        mViewModel.events.observe(this) { event ->
            handleAction(event.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(userManagerItemEvent: UserManagerItemEvent?) {
        when(userManagerItemEvent){
            is UserManagerItemEvent.SwitchPage -> {
                AppLog.d(TAG," --> User " + userManagerItemEvent.uname)
            }
        }
    }
    
    override fun onItemClickListener(view: View) {
    
    }
}