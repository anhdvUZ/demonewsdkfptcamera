package com.example.advmvvm.ui.user.userManager.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/23/2022.
 */
class UserManagerAdapter : RecyclerView.Adapter<UserManagerViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserManagerViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return UserManagerViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: UserManagerViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class UserManagerViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.userManagerItemViewModel, itemViewModel)
    }
}

@BindingAdapter("userManagerItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): UserManagerAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is UserManagerAdapter) {
        recyclerView.adapter as UserManagerAdapter
    } else {
        val bindableRecyclerAdapter = UserManagerAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}