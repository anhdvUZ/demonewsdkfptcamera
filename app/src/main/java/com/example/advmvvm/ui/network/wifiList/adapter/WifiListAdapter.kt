package com.example.advmvvm.ui.network.wifiList.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.databinding.FragmentCameraParamBinding
import com.example.advmvvm.ui.user.userManager.adapter.UserManagerAdapter

/**
 * Created by anhdv31 on 2/28/2022.
 */
class WifiListAdapter : RecyclerView.Adapter<WifiListViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WifiListViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return WifiListViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: WifiListViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class WifiListViewHolder(private  val binding: ViewDataBinding): RecyclerView.ViewHolder(binding.root){
    fun bind(baseItemViewModel : BaseItemViewModel){
        binding.setVariable(BR.wifiListItemViewModel,baseItemViewModel)
    }
}

@BindingAdapter("wifiListItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): WifiListAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is WifiListAdapter) {
        recyclerView.adapter as WifiListAdapter
    } else {
        val bindableRecyclerAdapter = WifiListAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}