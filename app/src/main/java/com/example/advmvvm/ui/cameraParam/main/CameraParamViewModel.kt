package com.example.advmvvm.ui.cameraParam.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.advmvvm.base.BaseItemViewModel
import com.example.advmvvm.base.Event
import com.example.advmvvm.ui.cameraParam.main.adapter.CameraParamItemViewModel
import com.example.advmvvm.ui.network.netCommonActive.adapter.NetCommonActiveItemViewModel
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.ui.videoEncode.encode.adapter.VideoEncodeItemViewModel
import com.lib.MTC.struct.CameraParam
import com.lib.MTC.struct.NetCommon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by anhdv31 on 2/28/2022.
 */
@HiltViewModel
class CameraParamViewModel @Inject constructor(
    private val cameraParamRepository: CameraParamRepository
):ViewModel() {
    val data : LiveData<List<BaseItemViewModel>>
        get() = _data
    private val _data = MutableLiveData<List<BaseItemViewModel>>(emptyList())
    
    val events : LiveData<Event<CameraParamEvents>>
        get() = _events
    private val _events = MutableLiveData<Event<CameraParamEvents>>()
    
    init {
        loadData()
    }
    
    private fun loadData() {
        viewModelScope.launch {
            val viewData = createViewModel(cameraParamRepository.getCameraParams())
            _data.postValue(viewData)
        }
    }
    
    private fun createViewModel(cameraParams: List<CameraParam>): List<BaseItemViewModel> {
        val viewData = mutableListOf<BaseItemViewModel>()
        cameraParams.forEach {
            viewData.add(CameraParamItemViewModel(it))
        }
        return viewData
    }
    
    open fun onApplyClicked(){
        val camParamNewConfig = (_data.value?.get(0) as CameraParamItemViewModel).cameraParam
        val res = cameraParamRepository.setCameraParam(camParamNewConfig)
        onApplyCameraParamDone(res)
        Log.d("CameraParamViewModel","onApplyClicked res " + res)
    }
    
    private  fun onApplyCameraParamDone(res : Int){
        _events.postValue(Event(CameraParamEvents.ApplyCameraParamConfig(res)))
    }
}

sealed class CameraParamEvents{
    data class ApplyCameraParamConfig(val result: Int) : CameraParamEvents()
}