package com.example.advmvvm.ui.videoColor.main.adapter

import android.util.Log
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.VideoColorControl

/**
 * Created by anhdv31 on 2/28/2022.
 */
class VideoColorItemViewModel(
    var videoColorControl: VideoColorControl
) : BaseItemViewModel {
    
    val gson = Gson()
    
    override val layoutId: Int = R.layout.item_video_color
    
    fun getData() = videoColorControl
    
    fun getVideoColorString(): String {
        Log.d("VideoColorItemViewModel", "ANHDV getVideoColorString")
        return gson.toJson(videoColorControl)
    }
    
    fun getVideoColorStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(videoColorControl)
    
    fun onVideoColorConfigChange(config: CharSequence) {
        Log.d("VideoColorItemViewModel", "ANHDV onVideoColorConfigChange" + config)
        try {
            videoColorControl = gson.fromJson(config.toString(), VideoColorControl::class.java)
        } catch (e: Exception) {
            Log.d("VideoColorItemViewModel", "ANHDV catch " + e.message)
            e.printStackTrace()
        }
    }
}