package com.example.advmvvm.ui.cameraParam.main

import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentCameraParamBinding
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CameraParamFragment : DataBindingBaseFragment<FragmentCameraParamBinding, BaseViewModel>(BaseViewModel::class.java) {
    private val mViewModel: CameraParamViewModel by viewModels()
    override fun getLayoutRes(): Int {
        return R.layout.fragment_camera_param
    }
    
    override fun initializeController() {
        binding.cameraParamViewModel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(cameraParamEvents: CameraParamEvents?) {
        when(cameraParamEvents){
            is CameraParamEvents.ApplyCameraParamConfig -> {
                val res = cameraParamEvents.result
                AppLog.d(TAG,"CameraParamEvent " + res)
                Toast.makeText(context,"CameraParamEvent " + res, Toast.LENGTH_LONG).show()
            }
        }
    }
}