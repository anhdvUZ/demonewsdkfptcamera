package com.example.advmvvm.ui.videoEncode.encode.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/23/2022.
 */
class VideoEncodeAdapter : RecyclerView.Adapter<VideoEncodeViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoEncodeViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return VideoEncodeViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: VideoEncodeViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class VideoEncodeViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root){
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.videoEncodeItemViewModel, itemViewModel)
    }
}

@BindingAdapter("videoEncodeItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels:List<BaseItemViewModel>?){
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): VideoEncodeAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is VideoEncodeAdapter) {
        recyclerView.adapter as VideoEncodeAdapter
    } else {
        val bindableRecyclerAdapter = VideoEncodeAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}