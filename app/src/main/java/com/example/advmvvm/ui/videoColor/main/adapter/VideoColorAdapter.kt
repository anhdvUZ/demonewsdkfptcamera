package com.example.advmvvm.ui.videoColor.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR
import com.example.advmvvm.base.BaseItemViewModel

/**
 * Created by anhdv31 on 2/28/2022.
 */
class VideoColorAdapter : RecyclerView.Adapter<VideoColorViewHolder>() {
    var itemViewModels: List<BaseItemViewModel> = emptyList()
    private val viewTypeToLayoutId: MutableMap<Int, Int> = mutableMapOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoColorViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), viewTypeToLayoutId[viewType] ?: 0,
                    parent,
                    false
                )
        return VideoColorViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder: VideoColorViewHolder, position: Int) {
        holder.bind(itemViewModels[position])
    }
    
    override fun getItemCount(): Int = itemViewModels.size
    
    override fun getItemViewType(position: Int): Int {
        val item = itemViewModels[position]
        if (!viewTypeToLayoutId.containsKey(item.viewType)) {
            viewTypeToLayoutId[item.viewType] = item.layoutId
        }
        return item.viewType
    }
    
    fun updateItems(items: List<BaseItemViewModel>?) {
        itemViewModels = items ?: emptyList()
        notifyDataSetChanged()
    }
}

class VideoColorViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(itemViewModel: BaseItemViewModel) {
        binding.setVariable(BR.videoColorItemViewModel, itemViewModel)
    }
}

@BindingAdapter("videoColorItemViewModels")
fun bindingItemViews(recyclerView: RecyclerView, itemViewModels: List<BaseItemViewModel>?) {
    val adapter = getOrCreateAdapter(recyclerView)
    adapter.updateItems(itemViewModels)
}

private fun getOrCreateAdapter(recyclerView: RecyclerView): VideoColorAdapter {
    return if (recyclerView.adapter != null && recyclerView.adapter is VideoColorAdapter) {
        recyclerView.adapter as VideoColorAdapter
    } else {
        val bindableRecyclerAdapter = VideoColorAdapter()
        recyclerView.adapter = bindableRecyclerAdapter
        bindableRecyclerAdapter
    }
}