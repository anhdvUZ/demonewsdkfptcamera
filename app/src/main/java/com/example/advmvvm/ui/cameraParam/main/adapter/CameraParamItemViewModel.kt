package com.example.advmvvm.ui.cameraParam.main.adapter

import com.example.advmvvm.R
import com.example.advmvvm.base.BaseItemViewModel
import com.google.gson.GsonBuilder
import com.lib.MTC.struct.CameraParam

/**
 * Created by anhdv31 on 2/28/2022.
 */
class CameraParamItemViewModel(val cameraParam: CameraParam) : BaseItemViewModel{
    override val layoutId: Int
        get() = R.layout.item_camera_param
    fun getCameraParamStringFormated() = GsonBuilder().setPrettyPrinting().create().toJson(cameraParam)
}