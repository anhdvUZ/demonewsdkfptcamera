package com.example.advmvvm.ui.videoWidget.main

import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.advmvvm.R
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.databinding.FragmentVideoWidgetBinding
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewEvent
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeViewModel
import com.example.advmvvm.utils.AppLog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoWidgetFragment : DataBindingBaseFragment<FragmentVideoWidgetBinding,BaseViewModel>(BaseViewModel::class.java) {
    
    private val mViewModel: VideoWidgetViewModel by viewModels()
    
    override fun getLayoutRes(): Int {
        return R.layout.fragment_video_widget
    }
    
    override fun initializeController() {
        binding.videoWidgetViewModel = mViewModel
        mViewModel.events.observe(this){
            handleAction(it.getContentIfNotHandled())
        }
    }
    
    private fun handleAction(videoEncodeViewEvent: VideoWidgetViewEvent?) {
        when(videoEncodeViewEvent){
            is VideoWidgetViewEvent.ApplyVideoWidgetConfig -> {
                val res = videoEncodeViewEvent.result
                AppLog.d(TAG,"VideoWidgetViewEvent " + res)
                Toast.makeText(context,"VideoWidgetConfig " + res, Toast.LENGTH_LONG).show()
            }
        }
    }
}