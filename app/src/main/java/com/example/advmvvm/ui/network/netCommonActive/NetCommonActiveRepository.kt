package com.example.advmvvm.ui.network.netCommonActive

import com.example.advmvvm.utils.AppLog
import com.google.gson.Gson
import com.lib.MTC.MTC
import com.lib.MTC.basic.Futils
import com.lib.MTC.struct.NetCommon

/**
 * Created by anhdv31 on 2/23/2022.
 */
class NetCommonActiveRepository {
    fun getNetCommonActives(): List<NetCommon> {
        val netCommons = arrayListOf<NetCommon>()
        val netCommon = NetCommon()
        val byteObj = Futils.ObjToBytes(netCommon)
        val byteParam = ByteArray(byteObj.size)
        val res: Int = MTC.DEV_GetNetCommon(MTC.loginID, byteParam)
        AppLog.d("NetCommonActiveRepository", "ANHDV NetCommonActiveRepository --> loginId " + MTC.loginID + " -- " + res)
        AppLog.d("NetCommonActiveRepository", "--> " + Gson().toJson(netCommon) + " -- " + res)
        if (res >= 0) {
            val retGetEncodeInfo = Futils.BytesToObj(netCommon, byteParam)
            netCommons.add(netCommon)
        }
        return netCommons
    }
    
    fun setNetCommonActives(netCommon: NetCommon): Int {
        val byteObj = Futils.ObjToBytes(netCommon)
        return -1
    }
}