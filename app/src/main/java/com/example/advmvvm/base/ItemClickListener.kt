package com.example.advmvvm.base

import android.view.View

/**
 * Created by anhdv31 on 2/14/2022.
 */
interface ItemClickListener {
    fun onItemClickListener(view: View)
}