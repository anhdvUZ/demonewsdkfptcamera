package com.example.advmvvm.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.XmlRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.preference.PreferenceFragmentCompat
import com.example.advmvvm.R

/**
 * Created by anhdv31 on 2/14/2022.
 */
abstract class PreferenceBaseFragment<VM:BaseViewModel>(private val viewModelClass: Class<VM>) : PreferenceFragmentCompat(){
    protected var TAG = javaClass.simpleName
    lateinit var viewModel: VM
    private fun getViewMD() : VM = ViewModelProvider(this).get(viewModelClass)

    @XmlRes
    abstract fun getXmlRes(): Int

    abstract fun initializeController()
    
    abstract fun initializePreferences()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewMD();
    }
    
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(getXmlRes(), rootKey)
        initializePreferences()
    }
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeController()
    }
    
    protected open fun getNavOptions(): NavOptions? {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()
    }
    
}