package com.example.advmvvm.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by anhdv31 on 1/25/2022.
 */
abstract  class BaseActivity : AppCompatActivity(){

    /*
    * @return int (R.layout.xxxx)
    * */
    abstract fun getLayoutRes(): Int

    abstract fun initializeComponents()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        initializeComponents()
    }
}