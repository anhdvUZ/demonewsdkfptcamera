package com.example.advmvvm.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.advmvvm.BR

/**
 * Created by anhdv31 on 2/22/2022.
 */
abstract class BaseAdapter<VH : BaseBindableViewHolder> : RecyclerView.Adapter<VH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding: ViewDataBinding =
            DataBindingUtil
                .inflate(
                    LayoutInflater.from(parent.context), getLayoutRes(),
                    parent,
                    false
                )
        return "a" as VH
    }
    
    override fun onBindViewHolder(holder: VH, position: Int) {
        TODO("Not yet implemented")
    }
    
    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }
    
    @LayoutRes
    abstract fun getLayoutRes() : Int
}

abstract class BaseBindableViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bind(itemViewModel: BaseItemViewModel)
}