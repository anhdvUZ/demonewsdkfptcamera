package com.example.advmvvm.utils

import android.util.Log

object AppLog {
    //public static final boolean D = BuildConfig.DEBUG;
    var D = Constants.IS_DEBUG;
    fun i(TAG: String?, msg: String?) {
        if (D) {
            Log.i(TAG, msg.toString())
        }
    }

    fun w(TAG: String?, msg: String?) {
        if (D) {
            Log.w(TAG, msg.toString())
        }
    }

    @JvmStatic
    fun d(TAG: String?, msg: String?) {
        if (D) {
            Log.d(TAG, msg.toString())
        }
    }

    fun e(TAG: String?, msg: String?) {
        if (D) {
            Log.e(TAG, msg.toString())
        }
    }
}