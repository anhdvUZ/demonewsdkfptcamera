package com.example.advmvvm.utils

import com.example.advmvvm.BuildConfig

/**
 * Created by anhdv31 on 2/17/2022.
 */
class Constants {
    companion object AccountInfo{
        @JvmStatic
        public var IS_DEBUG = BuildConfig.DEBUG
        //val uuid : String = "01234567a1b1c1dd"
        //val uuid : String = "01234560123456aa"
        val uuid : String = "01234567a1b1c1dd"
        val uname : String = "admin"
        val pwd : String = "888888"
    }
}