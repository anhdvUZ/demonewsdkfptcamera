package com.example.advmvvm.di

import com.example.advmvvm.ui.cameraParam.main.CameraParamRepository
import com.example.advmvvm.ui.home.adapter.HomeDataProvider
import com.example.advmvvm.ui.network.netCommonActive.NetCommonActiveRepository
import com.example.advmvvm.ui.network.wifiList.WifiListRepository
import com.example.advmvvm.ui.network.wifiManager.WifiManagerRepository
import com.example.advmvvm.ui.user.userManager.UserManagerRepository
import com.example.advmvvm.ui.videoColor.main.VideoColorRepository
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeFragment
import com.example.advmvvm.ui.videoEncode.encode.VideoEncodeRepository
import com.example.advmvvm.ui.videoEncode.encodeV2.VideoEncodeRepositoryV2
import com.example.advmvvm.ui.videoWidget.main.VideoWidgetRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by anhdv31 on 2/21/2022.
 */
@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {
    @Provides
    fun provideHomeDataProvider(): HomeDataProvider{
        return HomeDataProvider()
    }
    
    @Provides
    fun provideUserManagerRepository(): UserManagerRepository{
        return UserManagerRepository()
    }
    
    @Provides
    fun provideVideoEncodeFragment(): VideoEncodeRepository{
        return VideoEncodeRepository()
    }
    
    @Provides
    fun provideNetCommonActiveRepository(): NetCommonActiveRepository{
        return NetCommonActiveRepository()
    }
    
    @Provides
    fun provideWifiManagerRepository(): WifiManagerRepository {
        return WifiManagerRepository()
    }
    
    @Provides
    fun provideWifiListRepository(): WifiListRepository {
        return WifiListRepository()
    }
    
    @Provides
    fun provideCameraParamRepository(): CameraParamRepository {
        return CameraParamRepository()
    }
    
    @Provides
    fun provideVideoColorRepository(): VideoColorRepository{
        return VideoColorRepository()
    }
    
    @Provides
    fun provideVideoWidgetRepository(): VideoWidgetRepository {
        return VideoWidgetRepository()
    }
    
    @Provides
    fun provideVideoEncodeFragmentV2(): VideoEncodeRepositoryV2{
        return VideoEncodeRepositoryV2()
    }
}