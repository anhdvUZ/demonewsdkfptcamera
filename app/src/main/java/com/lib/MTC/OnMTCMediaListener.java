package com.lib.MTC;

public interface OnMTCMediaListener {
    void MediaFrameData(byte[] data, int size, int width, int height);
    void MediaInfo(int codecID, int width, int height);
    void MediaStart();
    void MediaSnapshot();
    void MediaRecordStart();
    void MediaStop();
    void MediaDuration(int duration);
    void MediaVideoList(String videoList);
    void MediaPlayTime(int playTime);
}
