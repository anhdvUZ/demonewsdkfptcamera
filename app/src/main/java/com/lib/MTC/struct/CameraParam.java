package com.lib.MTC.struct;

public class CameraParam {
    public int st_0_whiteBalance;
    public int st_1_dayNightColor; /*enum MTSP_DAY_NIGHT_MODE_E*/
    public int st_2_elecLevel;              /* Reference level value */
    public int st_3_apertureMode;  /* Auto iris mode */
    public int st_4_BLCMode;                /*1 Open ; 0 close (Backlight compensation mode)*/
    public int st_5_HLCMode;                /*1 Open ; 0 close (Highlight compensation mode)*/
    public Exposure st_6_exposure = new Exposure();
    public Gain st_7_gain = new Gain(); /* AGC */

    public int st_8_pictureFlip;   /*0 disable; 1 Enable (Flip image)*/
    public int st_9_pictureMirror; /*0 disable; 1 Enable (Mirror image)*/
    public int st_a0_rejectFlicker; /*0 disable; 1 Enable (Anti flash function of daylight lamp)*/
    public int st_b0_esShutter;     /*Electronic slow shutter function*/

    public int st_c0_ircutMode;  /*0 synchronous Switch; 1 Automatic Switch*/
    public int st_d0_bIrcutSwap; /*ircut Normal order = 0 ,Reverse order = 1*/

    public int st_e0_dncThr;        /*Dnc Threshold*/
    public int st_f0_AeSensitivity; /*AE Sensitivity*/

    public int st_g0_dayNTLevel; /* Noise filter Level, 0-5,0 does not filter, and the greater the 1-5 value, the more obvious the filtering effect */
    public int st_h0_nightNTLevel;

    public int st_i0_hightLight; /* Highlight suppression function, 0~255, defaults to 16*/
}
