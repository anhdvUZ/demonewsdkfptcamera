package com.lib.MTC.struct;

public class NetWifi {
    public boolean st_0_bEnable;
    public byte[] st_1_ssid = new byte[36];            // Wifi ssid
    public byte[] st_2_password = new byte[32];        // if exist, WiFi password
    public byte st_3_channel;    // channel
    public byte st_4_mode;       // refer to MTSP_AP_MODE_E
    public byte st_5_encrypType; // refer to MTSP_AP_ENCTYPE_E
    public IPAddress st_6_hostIP = new IPAddress();  // host ip
    public IPAddress st_7_submask = new IPAddress(); // netmask
    public IPAddress st_8_gateway = new IPAddress(); // gateway
}
