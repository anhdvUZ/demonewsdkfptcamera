package com.lib.MTC.struct;

public class MotionDetect {
    public int st_0_bEnable;                               /* 1: Enable; 0: Disable */
    public int st_1_level;                                 /* The Sensitivity refer to MTSP_MD_SENSI_LEVEL_E*/
    public int[] st_2_region = new int[32]; /**/
    public EventHandle st_3_hEvent = new EventHandle();                /**/
    public int st_4_reserved;                              // Feature
}
