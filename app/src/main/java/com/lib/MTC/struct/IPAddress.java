package com.lib.MTC.struct;

public class IPAddress {
    public byte [] st_0_c = new byte[4];

    public String ToString() {
        return String.format("%d", (long)st_0_c[0] & 0xff) + "." + String.format("%d", st_0_c[1] & 0xff) + "." + String.format("%d", st_0_c[2] & 0xff) + "." + String.format("%d", st_0_c[3]&0xff);
    }
}
