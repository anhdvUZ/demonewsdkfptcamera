package com.lib.MTC.struct;
public class VideoFormat {
    public int st_0_compression;    //enum MTSP_CAPTURE_COMP_E
    public int st_1_resolution;     //enum MTSP_CAPTURE_SIZE_E
    public int st_2_bitRateControl; //enum MTSP_CAPTURE_BRC_E
    public int st_3_quality;        // 15 - 51
    public int st_4_FPS;            // 6 - 25/30
    public int st_5_bitRate;        // 250 - 4096
    public int st_6_GOP;            // 2 - 12
}
