package com.lib.MTC.struct;

public class VideoColorParam {
    public int st_0_brightness;   //	0-100	default 50
    public int st_1_contrast;     //	0-100	default 50
    public int st_2_saturation;   //	0-100	default 50
    public int st_3_hue;          //	0-100	default 50
    public int st_4_gain;         //	0-100	default 50
    public int st_5_whitebalance; //  0x0,0x1,0x2
    public int st_6_acutance;     //   0-15
}
