package com.lib.MTC.struct;

public class VideoColor {
    public TimeSection st_0_timeSection = new TimeSection(); // time
    public VideoColorParam st_1_color = new VideoColorParam();   // color
    public int st_2_bEnable;                    // 0: infrared on; 1: infrared off
}
