package com.lib.MTC.struct;

public class NetDHCP {
    public int st_0_bEnable;
    public byte[] st_1_name = new byte[32];

    public byte[] getSt_1_name() {
        return st_1_name;
    }

    public void setSt_1_name(byte[] st_1_name) {
        this.st_1_name = st_1_name;
    }
}
