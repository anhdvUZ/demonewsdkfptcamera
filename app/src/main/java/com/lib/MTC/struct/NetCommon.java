package com.lib.MTC.struct;

public class NetCommon {
    public byte[] st_0_hostName = new byte[64];
    public IPAddress st_1_hostIP = new IPAddress();
    public IPAddress st_2_submask = new IPAddress();
    public IPAddress st_3_gateway = new IPAddress();
    public int st_4_httpPort;
    public int st_5_tcpPort;
    public int st_6_udpPort;
    public int st_7_sslPort;
    public int st_8_maxConn;
    public int st_9_monMode; /* {"TCP","UDP","MCAST"} */
    public int st_a_maxBps;
    public int st_b_transferPlan;    /* Quality ,Adaptive, transmission*/
    public int st_c_bUseHSDownLoad; /* High speed download */
    public byte[] st_d_mac = new byte[32];
}
