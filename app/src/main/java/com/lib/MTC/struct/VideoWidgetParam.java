package com.lib.MTC.struct;

public class VideoWidgetParam {
    public int st_0_frontground; // RGB color
    public int st_1_background;  // RGB color
    public Rect st_2_relativePos = new Rect();  // posiston
    public int st_3_bPreviewBlend;        //
    public int st_4_bEncodeBlend;         //
}
