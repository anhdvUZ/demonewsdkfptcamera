package com.lib.MTC.struct;

public class VideoWidget {
    public int st_0_coverNum; // The number cover (covers)
    public VideoWidgetParam[] st_1_covers = new VideoWidgetParam[4];
    public VideoWidgetParam st_2_channelTitle = new VideoWidgetParam();
    public VideoWidgetParam st_3_timeTitle = new VideoWidgetParam();

    public byte[] st_4_channelName = new byte[64];

    public VideoWidget()
    {
        int i;
        for (i = 0; i < st_1_covers.length; i++)
        {
            st_1_covers[i] = new VideoWidgetParam();
        }
    }

}
