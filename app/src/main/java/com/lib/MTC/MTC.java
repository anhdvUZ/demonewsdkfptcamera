package com.lib.MTC;

public class MTC {
    // Private value

    public static int loginID = -1;
    // MT codec type
    public static final int MT_CODEC_TYPE_NONE = 0;
    public static final int MT_CODEC_TYPE_H264 = 96;


    // Media Event frame
    /* The data is not ready for receiving yet. */
    public static final int MT_MEDIA_EVENT_NOREADY = 0;
    /* The data is video. */
    public static final int MT_MEDIA_EVENT_VIDEO = 1;
    /* The data is audio. */
    public static final int MT_MEDIA_EVENT_AUDIO = 2;
    /* The data is play time */
    public static final int MT_MEDIA_EVENT_PLAYTIME = 3;
    /* The data is not enough buffer size, resize for buffer. */
    public static final int MT_MEDIA_EVENT_RESIZE = 4;
    /* The data is start stream. */
    public static final int MT_MEDIA_EVENT_PLAY = 5;
    /* The data is end stream. */
    public static final int MT_MEDIA_EVENT_END = 6;
    /* The data is duration. */
    public static final int MT_MEDIA_EVENT_DURATION = 7;
    /* The data is media info. */
    public static final int MT_MEDIA_EVENT_INFO = 8;

    public static void registerOnDeviceListener(int loginID, OnMTCLoginListener listener){
        DEV_SetOnLoginListener(loginID, listener);
    }

    public static void UnRegisterOnDeviceListener(int loginID){
        DEV_RmOnLoginListener(loginID);
    }


    public static void DeviceRelease(int loginID)
    {
        MTC.DEV_StopPlay(loginID);
        MTC.UnRegisterOnDeviceListener(loginID);
        MTC.DEV_Logout(loginID);
    }

    /*******************************************************************************************
     * Set Callback Login Status
     * *****************************************************************************************/
    /* Set login listener status  */
    public native static int DEV_SetOnLoginListener(int loginID, OnMTCLoginListener listener);
    /* Remove login listener status  */
    public native static int DEV_RmOnLoginListener(int loginID);

    public native static void SetDebug();

    public native static void SDK_init();

    public native static int DEV_LoginCloud(String uuid, String username, String password);

    public native static int DEV_Logout(int loginID);

    public native static int DEV_GetUserInfo(int loginID, String[] result);

    public native static int DEV_ModifyPassword(int loginID, String username, String password, String newPassword);

    public native static int DEV_GetConfigEncode(int loginID, byte[] param);

    public native static int DEV_SetConfigEncode(int loginID, byte[] param);

    public native static int DEV_GetNetCommon(int loginID, byte[] param);

    public native static int DEV_GetNetWifiInfo(int loginID, byte[] param);

    public native static int DEV_SetNetWifi(int loginID, byte[] param);

    public native static int DEV_GetNetWifiList(int loginID, byte[] param);

    public native static int DEV_GetVideoWidget(int loginID, byte[] param);

    public native static int DEV_SetVideoWidget(int loginID, byte[] param);

    public native static int DEV_GetCameraParam(int loginID, byte[] param);

    public native static int DEV_SetCameraParam(int loginID, byte[] param);

    public native static int DEV_GetVideoColor(int loginID, byte[] param);

    public native static int DEV_SetVideoColor(int loginID, byte[] param);

    public native static int DEV_GetMotionDetect(int loginID, byte[] param);

    public native static int DEV_SetMotionDetect(int loginID, byte[] param);

    public native static int DEV_SetPlayHandle(int loginID, int playHandle);

    public native static int DEV_StartRealPlay(int loginID, byte[] param);

    public native static int DEV_AudioSend(int loginID, byte[] data, int size);

    public native static int DEV_StopPlay(int loginID);

    public native static int DEV_FindEventList(int loginID, byte[] param, byte[] result);

    public native static int DEV_PlayRecordStart(int loginID, byte[] param);

    public native static int DEV_PlayRecordStop(int loginID);


    /* ******************************************************************
    Play stream media
    * *******************************************************************/
    public native static int Play_OpenStream();

    public native static int Play_CloseStream(int playHandle);

    public native static int Play_GetVideoFrame(int playHandle, int[] type, byte[] data, int maxSize, int[] outSize, int[] info);

    public native static int Play_MediaInfoParse(byte[] data, int size, int[] bSeek, int[] duration, int[] fps, int[] width, int[] height, int[] codec, byte[] sps, int[] nLenSps, byte[] pps, int[] nLenPps);

    public native static int Play_SetOnMediaListener(int playHandle, OnMTCMediaListener listener);

    public native static int Play_RmOnMediaListener(int playHandle);

    public native static int Play_Start(int playHandle);

    public native static int Play_CloudSearchByTime(int playHandle, String serial, String start_time, String Stop_time, String[] VideoList);

    public native static int Play_CloudUrlStart(int playHandle, String url);


//    public static void onResponse(int agv1)
//    {
//        System.out.printf("phucnv17 callback public static response: %d\n", agv1);
//    }

    static {
        try {
            System.loadLibrary("MTC");
            System.out.println("MTC loadLibrary success\n");
        } catch (UnsatisfiedLinkError ule) {
            System.out.println("loadLibrary(MTC), +++++++++++++++++++++++++++++++++++++++++++ error []" + ule.getMessage());
        }
    }
}
