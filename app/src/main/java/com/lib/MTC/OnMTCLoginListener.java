package com.lib.MTC;

public interface OnMTCLoginListener {

    void onLoginSuccess();

    void onLoginError(int status);

}
