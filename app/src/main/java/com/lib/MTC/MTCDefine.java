package com.lib.MTC;

public class MTCDefine {
    /* Define all enum element */

    /*Capture compression mode*/
    public static final int MTSP_CAPTURE_COMP_H264      = 0;
    public static final int MTSP_CAPTURE_COMP_H265      = 1;

    /*capture size image*/
    public static final int MTSP_CAPTURE_SIZE_VGA = 0;        // 640*480(PAL)		640*480(NTSC)
    public static final int MTSP_CAPTURE_SIZE_D1  = 1;        // 720*576(PAL)		720*480(NTSC)   800*480
    public static final int MTSP_CAPTURE_SIZE_720P = 2;       // 1280*720
    public static final int MTSP_CAPTURE_SIZE_1080P = 3;      // 1920*1080

    /*BitRate Control*/
    public static final int  MTSP_CAPTURE_BRC_CBR = 0;
    public static final int  MTSP_CAPTURE_BRC_VBR = 1;
    public static final int  MTSP_CAPTURE_BRC_MBR = 2;
}
