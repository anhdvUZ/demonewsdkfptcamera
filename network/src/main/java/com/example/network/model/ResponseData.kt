package com.example.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by anhdv31 on 2/14/2022.
 */
class ResponseData<T> {
    @SerializedName("data")
    var data: List<T>? = null

    @SerializedName("error")
    var error: List<T>? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("result")
    var result = false

    @SerializedName("code_status")
    var code_status = 0
}