package com.example.registration.ui

import android.view.View
import androidx.navigation.Navigation
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.advmvvm.base.ItemClickListener
import com.example.registration.R
import com.example.registration.databinding.FragmentLoginBinding

class LoginFragment : DataBindingBaseFragment<FragmentLoginBinding,BaseViewModel>(BaseViewModel::class.java),ItemClickListener {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_login;
    }

    override fun initializeController() {
        binding.lifecycleOwner = this
        binding.itemClickListener = this
    }
    
    override fun onItemClickListener(view: View) {
        when(view.id){
            R.id.tv_forgot->{
                Navigation
                    .findNavController(binding.tvForgot)
                    .navigate(R.id.action_loginFragment_to_forgotFragment,null,getNavOptions())
            }
        }
    }
    
}