package com.example.registration.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.advmvvm.base.BaseViewModel
import com.example.advmvvm.base.DataBindingBaseFragment
import com.example.registration.R
import com.example.registration.databinding.FragmentForgotPasswordBinding

class ForgotPasswordFragment : DataBindingBaseFragment<FragmentForgotPasswordBinding,BaseViewModel>(BaseViewModel::class.java) {
    override fun getLayoutRes(): Int {
        return R.layout.fragment_forgot_password
    }
    
    override fun initializeController() {
        binding.lifecycleOwner = this
    }
}