package com.example.registration

import com.example.advmvvm.base.BaseActivity

class RegisterActivity : BaseActivity() {
    override fun getLayoutRes(): Int {
        return R.layout.activity_register
    }

    override fun initializeComponents() {}
}